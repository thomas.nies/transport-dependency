import pandas as pd
import os

# Change directory to the directory of the script
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

corr_vals = pd.read_csv("data/corr_vals.csv") 
p_vals = pd.read_csv("data/p_vals.csv") 

# just as a reminer of how things where saved 
funcs_name = ["graph_dist", "corr_dist"]
coefs_name = ["tcor", "mtcor", "tcor_approx", "dcor"]
data_sets_name = ["bin", "non-bin"]

key = " ".join(["tcor", "non-bin", "corr_dist"])


print("Values for table 1")
print(corr_vals[key], p_vals[key])


