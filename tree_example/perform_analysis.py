# This script requires BioPython and Pandas to be installed.

import numpy as np
import sys
from Bio import Phylo
import csv
import pandas as pd
import time
import os

import transport_dependency as td
from transport_dependency import tcor, mtcor, permutation_test
from transport_dependency.extra_tools.other_coeffs import dcor

# Change directory to the directory of the script
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

samples = np.arange(98)   # index identifying each sample

################### defining the distance on the tree 


tree = Phylo.read("data/tree.Newick", "newick") # reading the data of the tree derived in [1]
sample_labels = ["Sample_" + str(i+1) for i in range(100) if i not in [73, 81]] # the samples 73 and 81 are missing in the original study as well

def shortest_path_tree(tree, target1, target2=None):
    """ Calculate number of edges to travel from target1 to target2 (shortest path distance). """
    if target2 is None:
        return sum(1 for n in tree.get_path(target1) if n.branch_length is not None)
    mrca = tree.common_ancestor(target1, target2)
    return shortest_path_tree(mrca, target1) + shortest_path_tree(mrca, target2)

d_x = np.empty((98, 98))
for i in range(len(sample_labels)):
    for j in range(len(sample_labels)):
        target1 = sample_labels[i]
        target2 = sample_labels[j]
        d_x[i, j] = shortest_path_tree(tree, target1, target2)

# wrapper function that accepts indicies as input
def d_func(i, j):
    return d_x[i, j]


################### defining the cost distance based on the correlation measure


with open('data/correlation_cost_matrix_x.csv') as f:
    cost = np.empty((98, 98))
    reader = csv.reader(f)
    data = list(reader)
    for i in range(1, len(data)):
        for j in range(1, len(data[i])):
            cost[i-1, j-1] = float(data[i][j])
# we need to consider c as a function for the permutatoin test
def c_func(i, j):
    return cost[i, j]

if __name__ == "__main__":
    ################### Loadig the remaining datasets (which was already processed in the get_data.py file)
    print(tcor(samples, samples, d_x=c_func, d_y=d_func))
    
    
    non_bin_dat = pd.read_csv('data/processed_data.csv') 
    bin_dat = pd.read_csv('data/binary_processed_data.csv') 
    phenotypes = ["Brca1.mutation", "ERp", "grade", "Lymphocytic.Infiltrate", "Angioinvasion", "metastases"]
    
    
    ################### Begin of computations
    
    np.random.seed(0) 
    
    funcs = [d_func, c_func]
    funcs_name = ["graph_dist", "corr_dist"]
    
    coefs = [tcor, mtcor, dcor]
    coefs_name = ["tcor", "mtcor", "dcor"]
    
    data_sets = [non_bin_dat, bin_dat]
    data_sets_name = ["non-bin", "bin"]
    
    corr_vals = pd.DataFrame()
    p_vals = pd.DataFrame()
    averages_under_null_hypothesis = pd.DataFrame()
    
    # six phenotipes considered: Brac, ERp, cancer grade, angiinvasion, methastasis, lymphatic infiltration.
    coef = tcor
    for i in range(len(data_sets)):
        dat = data_sets[i]
        for j in range(len(funcs)):
            func = funcs[j]
            for k in range(len(coefs)):
                coef = coefs[k]
                experiment_setting = " ".join([coefs_name[k], data_sets_name[i], funcs_name[j]])
                corr_lst = []
                p_val_lst = []
                average_under_null_hypothesis_lst = []
                print(experiment_setting)
                t_0 = time.time()
                for pheno in phenotypes:
                    current_phenotype = np.array(dat[pheno])
                    I = np.where(~np.isnan(current_phenotype))
                    res = permutation_test(current_phenotype[I], samples[I], coef=coef, m=999, level=0.05, use_multiprocessing=True, exact_level=True, d_y=func)
                    print(res)
                    corr = res["test_stat"]
                    p_val = res["p_val"]
                    test_res= res["test_res"]
                    average_under_null_hypothesis = res["average_under_null_hypothesis"]
                    corr_lst.append(corr)
                    p_val_lst.append(p_val)
                    average_under_null_hypothesis_lst.append(average_under_null_hypothesis)
                t_1 = time.time()
                print("total time: ", t_1 - t_0)
                print("/n")
                corr_vals[experiment_setting] = corr_lst
                p_vals[experiment_setting] = p_val_lst
                averages_under_null_hypothesis[experiment_setting] = average_under_null_hypothesis_lst
    corr_vals.to_csv("data/corr_vals.csv")
    p_vals.to_csv("data/p_vals.csv")
    averages_under_null_hypothesis.to_csv("data/averages_under_null_hypothesis.csv")

"""
[1] Testing for dependence on tree structures, Merle Behr, M. Azim Ansari, Axel Munk, Chris Holmes,
Proceedings of the National Academy of Sciences May 2020, 117 (18) 9787-9792; DOI: 10.1073/pnas.1912957117
"""
