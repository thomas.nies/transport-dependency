import numpy as np
import pandas as pd
import os

# Change directory to the directory of the script
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

###### Process and save phenotipe data, in binary and non-binary.
###### The data processing corresponds exactly to the processing procedure used in [1]
###### The phenotypes considered are Brca1.mutation, ERp, grade, Lymphocytic.Infiltrate, Angioinvasion and metastases

data_frame = pd.read_csv('data/our_genes.csv') # data frame imported from R
processed_data = pd.DataFrame()
binary_processed_data = pd.DataFrame()

# Brca1.mutation data
print("\nBrca1.mutation\n")
brca = data_frame["Brca1.mutation"].copy()
brca[brca == 'BRCA2'] = 1
bin_brca = brca.copy()
print(np.array(brca))
    
# ERp data
print("\nERp\n")
erp = data_frame["ERp"].copy()
bin_erp = erp.copy()
bin_erp[erp > 0] = 1
print(np.array(erp))
print(np.array(bin_erp))

# cancer grades data
print("\ngrade\n")
grade = data_frame["grade"].copy()
bin_grade = grade.copy()
bin_grade[grade <= 2] = 0
bin_grade[grade == 3] = 1
print(np.array(grade))
print(np.array(bin_grade))

# lymphatic infiltration
print("\nLymphocytic.Infiltrate\n")
lymph_inf = data_frame["Lymphocytic.Infiltrate"].copy()
bin_lymph_inf = lymph_inf.copy()
print(np.array(lymph_inf))

# angioinvasion
print("\nAngioinvasion\n")
angio_inv = data_frame["Angioinvasion"].copy()
bin_angio_inv  = angio_inv.copy()
print(np.array(angio_inv))
print(np.array(bin_angio_inv))

# metastasis
print("\nmetastases\n")
metastases = data_frame["metastases"].copy()
metastases[metastases == "NA"] = np.nan
bin_metastases  = metastases.copy()
print(np.array(metastases))


################# Saving the binary and non-binary data in two new dataframes
data = pd.DataFrame()
data["Brca1.mutation"] = brca
data["ERp"] = erp
data["grade"] = grade 
data["Lymphocytic.Infiltrate"] = lymph_inf
data["Angioinvasion"] = angio_inv
data["metastases"] = metastases

data.to_csv("data/processed_data.csv")

bin_data = pd.DataFrame()
bin_data["Brca1.mutation"] = bin_brca
bin_data["ERp"] = bin_erp
bin_data["grade"] = bin_grade 
bin_data["Lymphocytic.Infiltrate"] = bin_lymph_inf
bin_data["Angioinvasion"] = bin_angio_inv
bin_data["metastases"] = bin_metastases

bin_data.to_csv("data/binary_processed_data.csv")


"""
[1] Testing for dependence on tree structures, Merle Behr, M. Azim Ansari, Axel Munk, Chris Holmes,
Proceedings of the National Academy of Sciences May 2020, 117 (18) 9787-9792; DOI: 10.1073/pnas.1912957117
"""
