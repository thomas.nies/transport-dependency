import pandas as pd

corr_vals = pd.read_csv("data/corr_vals.csv") 
p_vals = pd.read_csv("data/p_vals.csv") 

# just as a reminer of how things where saved 
funcs_name = ["graph_dist", "corr_dist"]
coefs_name = ["tcor", "mtcor"]
data_sets_name = ["bin", "non-bin"]

key1 = " ".join(["tcor", "bin", "graph_dist"])
key2 = " ".join(["mtcor", "bin", "graph_dist"])

print("The difference between the values of tcor and mtcor on binary data are essentially 0:")
print(corr_vals[key1]- corr_vals[key2])

key1 = " ".join(["mtcor", "non-bin", "graph_dist"])
key2 = " ".join(["tcor", "non-bin", "graph_dist"])

# The difference between the values of tcor and mtcor on quantitative data is significant:
print("The difference between the values of tcor and mtcor on quantitative data is significant when the variable is not binary (such as for ERp):")
print(corr_vals[key1][1]- corr_vals[key2][1])
