import numpy as np
from scipy.spatial.distance import pdist, squareform
from .min_cost_flow import scale_to_int, min_cost_flow_ortools, ot_solver
import ot
import warnings
warnings.simplefilter

def tcor(x, y, p=1, r=1, alpha=None, d_x='euclidean', d_y='euclidean', scale=True,
        estimation_method="sample", solver="pot", s_for_ind_est=1):
    """
    Compute the transport correlation.

    Parameters
    ----------
    x : array_like
    y : array_like
    p : float
        determines the power to be used 
    r : float
        detemines the r-product metric to be used. This is one by default
    alpha : float
        if alpha is None then alpha* is computed
    d_x : function 
        The distance function for the first marginal space,
        needs to be at least symmetric
    d_y : function 
        The distance function for the second marginal space,
        needs to be at least symmetric
    scale : bool
        if scale is False, then the transport dependency is computed,
        without any scaling to [0, 1]
    estimation_method : str
        Parameter specifiying which esimation method is prefered.
        The default is "sample", that means that the independent coupling 
        is given by the product of the marginas of gamma.
        Other schemes involve random permutations or random samples.
    solver : function
        the optimal transport solver to be used
    s_for_ind_est : int
        If a random estimator is used then this specifies the number of subsamples or permutations to be used.

    Returns
    -------
    transport correlation : float
        
    Examples
    --------
    >>> import numpy
    >>> d = 2
    >>> n = 20 
    >>> x = np.random.multivariate_normal(np.zeros(d), np.eye(d), n)
    >>> y = 3 * x + [2, 7]
    >>> res = tcor(x, y)
    >>> print(res)

    Reference
    --------
    [1] Transport Dependency: Optimal Transport Based Dependency Measures,
        Thomas Giacomo Nies, Thomas Staudt and Axel Munk, 2021
    """
    x = np.asarray(x)
    y = np.asarray(y)

    if len(x.shape)==1:
        x = x[:, np.newaxis]
    if len(y.shape)==1:
        y = y[:, np.newaxis]

    n = len(x)

    d_x_mat = compute_cost_matrix(x, d_x)
    d_y_mat = compute_cost_matrix(y, d_y)

    # costs are scaled appropriately
    gamma = np.eye(n)/n
    mu, nu = marginals(gamma)
    pdiam_x = diameter(d_x_mat, mu, p)
    pdiam_y = diameter(d_y_mat, nu, p)

    if alpha is None: # determine alpha_star
        alpha = pdiam_y/pdiam_x

    if scale:
        h = lambda dx, dy : ((alpha*dx)**r+(dy**r))**(p/r) / pdiam_y
    else:
        h = lambda dx, dy : ((alpha*dx)**r + (dy**r))**(p/r)

    # determine the solver to use
    if solver == "best":
        if estimation_method!="sample":
            solver = "pot"
        else:
            if (p==1) and (y.shape[1]==1) and (d_y == "euclidean"):
                solver = "pivot"
            if (p==1) and (x.shape[1]==1) and (d_x == "euclidean"):
                solver = "inverted pivot"
            else:
                solver = "pot"
    transport_cost = td(d_x, d_y, h, x, y, gamma=None,
            estimation_method=estimation_method, solver=solver,
            s_for_ind_est=s_for_ind_est, adjust_support=True)
    res = transport_cost**(1/p) 
    return res

def td(c_x, c_y, h, x, y, gamma=None, estimation_method="sample", solver="pot",
        s_for_ind_est=1, adjust_support=True):
    """
    Compute the transport dependency for costs of the form h(c_x, c_y).

    Parameters
    ----------
    c_x : function
        the cost function on the first marginal space 
    c_y : function
        the cost function on the second marginal space 
    h : function
        a function from R^2 to R.
    x : array_like
        the points on which the first marginal of gamma is supported
    y : array_like
        the points on which the second marginal of gamma is supported
    gamma : numpy array
        the joint distribution, if None then we consider the empirical measure
        for data ((x[i], y[i]))_{i = 1, \dots, n}, with n = len(x).
    estimation_method : str
        Parameter specifiying which esimation method is prefered.
        The default is "sample", that means that the independent coupling 
        is given by the product of the marginas of gamma.
        Other schemes involve random permutations or random samples.
    solver : function/string
        Optimal transport solver to be used to solve the problem
    adjust_support : bool
        Indicates weather it is necessary to adjust the marginal spaces, so that
        x and y do not contain duplicates. cost matrices and gamma are then 
        adjusted accordingly as well.
    s_for_ind_est : int
        number of permutations/resamples used for the esimation of the 
        independent coupling
        other arguments for alternative estimation methods

    Returns
    -------
    transport dependency value: float

    """
    x = np.asarray(x)
    y = np.asarray(y)

    if len(x.shape)==1:
        x = x[:, np.newaxis]
    if len(y.shape)==1:
        y = y[:, np.newaxis]

    # first we compute gamma 
    if gamma is None:
        n = len(x)
        gamma = np.eye(n, dtype=int)

    # further it might be sometimes sensible to reduce the dimensions of gamma
    # by removing duplicates in x and y and changing gamma accordingly
    if adjust_support:
        x, y, gamma = remove_doubles(x, y, gamma)

    # then we estimate the independet coupling
    if estimation_method == "sample":
        ind = independent_coupling(gamma) 
    elif estimation_method == "permutation":
        ind = permutation_estimator(gamma, s_for_ind_est) 
    elif estimation_method == "sub_sample":
        ind = sub_sample_esitmator(gamma, s_for_ind_est)
    else:
        raise Exception("method not implemented")

    # finally we give "gamma" and "ind" equal mass (which they had not so far!)
    mass_gamma = gamma.sum()
    mass_ind = ind.sum()

    # we really need the problem to be an integer probelm
    # when using min cost flow based algorithms
    if (gamma.dtype==int)  and (ind.dtype==int):
        gcd = np.gcd(mass_gamma, mass_ind)
        total_mass = np.lcm(mass_gamma, mass_ind)
        gamma = gamma*(mass_ind//gcd)
        ind = ind*(mass_gamma//gcd)
    else: 
        gamma = gamma/mass_gamma
        ind = ind/mass_ind
        total_mass = 1

    # now we remove all that anoying points of mass 0
    I1, J1 = np.where(gamma > 0)
    I2, J2 = np.where(ind > 0)
    gamma_pos = gamma[I1, J1]
    ind_pos = ind[I2, J2]

    # compute the marginal costs
    cxm = compute_cost_matrix(x, c_x)
    cym = compute_cost_matrix(y, c_y)

    # Now we can solve the ot problem in in various ways
    if solver == "pot":
        c = h(cxm[I1[:, np.newaxis], I2[np.newaxis, :]], cym[J1[:, np.newaxis], J2[np.newaxis, :]])
        res = pot_solver(gamma_pos, ind_pos, c)
    elif solver == "pivot":
        # notice that this check cannot verify weather h is linear 
        check_if_pivot_applies(y, c_y, gamma)
        hcxm = h(cxm, 0)
        hcym = h(0, cym)
        res = td_with_pivot(hcxm, hcym, gamma, ind)
    elif solver == "inverted pivot":
        # notice that this check cannot verify weather h is linear 
        check_if_pivot_applies(x, c_x, gamma)
        hcxm = h(cxm, 0)
        hcym = h(0, cym)
        res = td_with_pivot(hcym, hcxm, gamma.T, ind.T)
    elif solver == "grid":
        # notice that this check cannot verify weather h is linear 
        hcxm = h(cxm, 0)
        hcym = h(0, cym)
        check_if_grid_applies(x, y, c_x, c_y, gamma)
        res = td_on_grid(hcxm, hcym, gamma, ind)
    else:
        c = h(cxm[I1[:, np.newaxis], I2[np.newaxis, :]], cym[J1[:, np.newaxis], J2[np.newaxis, :]])
        res = solver(gamma_pos, ind_pos, c)

    # Finally we need to rescale to the correct mass level
    res = res/total_mass

    # and we are done :)
    return res

def check_if_pivot_applies(y, d_y, gamma):
        y_sorted = all(y == np.sort(y, axis=0))
        dim_y_is_one = y.shape[1]==1
        d_y_is_euclidean = d_y=="euclidean"
        gamma_is_int = gamma.dtype==int
        applicable = y_sorted and dim_y_is_one and d_y_is_euclidean and gamma_is_int
        if not applicable:
            raise Warning("Using a pivot based grid algorithm might be wrong in this case")

def check_if_grid_applies(x, y, d_x, d_y, gamma):
        y_sorted = all(y == np.sort(y, axis=0))
        x_sorted = all(x == np.sort(x, axis=0))
        dim_y_is_one = y.shape[1]==1
        dim_x_is_one = x.shape[1]==1
        d_y_is_euclidean = d_y=="euclidean"
        d_x_is_euclidean = d_x=="euclidean"
        gamma_is_int = gamma.dtype==int
        applicable_x = x_sorted and dim_x_is_one and d_x_is_euclidean and gamma_is_int
        applicable_y = y_sorted and dim_y_is_one and d_y_is_euclidean and gamma_is_int
        if not (applicable_x and applicable_y):
            raise Warning("Using this grid algorithm might be wrong in this case")

def remove_doubles(x, y, gamma):
    """function to build the sparse distribution matrix gamma, modulo its total mass,
    which might be arbitrary and still has to be scaled back to 1."""
    n = len(x)
    m = len(y)
    g_type = gamma.dtype
    x_u, ind_x, ind_x_inv = np.unique(x, axis=0, return_index=True, return_inverse=True)
    y_u, ind_y, ind_y_inv = np.unique(y, axis=0, return_index=True, return_inverse=True) 
    # build matrix gamma, first we sum up all entries that share the same y value
    new_gamma = np.zeros((len(x), len(y_u)), dtype=g_type)
    for i in range(m):
        np.add.at(new_gamma[i], ind_y_inv, gamma[i])

    new_new_gamma = np.zeros((len(x_u), len(y_u)), dtype=g_type)
    for j in range(len(y_u)):
        np.add.at(new_new_gamma[:, j], ind_x_inv, new_gamma[:, j])

    return x_u, y_u, new_new_gamma

def independent_coupling(gamma):
    """Computes the sample estimator of the independent coupling, up to rescaling to 1. """
    # build matrix gamma
    mu, nu = marginals(gamma)
    ind = mu.reshape(-1, 1) * nu.reshape(1, -1)
    return ind

def permutation_estimator(int_gamma, s):
    """Computes the permutation estimator of the independent coupling, up to rescaling to 1."""
    if int_gamma.dtype != int:
        raise Exception("Gamma needs to be integer")
    n = int_gamma.sum()
    mu, nu = marginals(int_gamma)
    ind_x = np.repeat(np.arange(int_gamma.shape[0], dtype=int), mu)
    ind_y = np.repeat(np.arange(int_gamma.shape[1], dtype=int), nu)
    ind = np.zeros_like(int_gamma, dtype=int)
    for _ in range(s):
        perm_y = np.random.permutation(n)
        for k in range(n):
            i, j = ind_x[k], ind_y[perm_y[k]]
            ind[i, j] += 1
    return ind

def sub_sample_esitmator(int_gamma, s, replace=True):
    """Computes the sample estimator of the independent coupling, up to rescaling to 1. It is the
    empirical measure for s*n samples, which are taken from the product of the marginal empirical
    distributions."""
    ind = independent_coupling(int_gamma)
    n = int_gamma.sum()
    ns = n*s
    mu, nu = marginals(int_gamma)
    index_x = np.repeat(np.arange(int_gamma.shape[0], mu))
    index_y = np.repeat(np.arange(int_gamma.shape[1], nu))
    ind = np.zeros_like(int_gamma, dtype=int)
    for _ in range(s):
        samples = np.random.choice(n, size=n, replace=replace)
        for k in samples:
            i, j = ind_x[k], ind_y[perm_y[k]]
            ind[i, j] += 1
    return ind

def pot_solver(a, b, c):
    """Shorthand wrapper for ot.emd2"""
    # here we make sure not to have integers
    a = a/1
    b = b/1
    res = ot.emd2(a, b, c, numItermax=1000000)
    return res

def integer_pot_solver(a, b, c):
    message = "Input histogram consists of integer. "+ "The transport plan will be casted accordingly, possibly resulting in a loss of " + "precision. If this behaviour is unwanted, please make sure your input histogram " + "consists of floating point elements."
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", message=message)
        res = ot.emd2(a, b, c, numItermax=1000000)
    return res


def td_on_grid(d_x, d_y, gamma_int, ind_int):
    n_x = d_x.shape[0] 
    n_y = d_y.shape[0]
    supplies = (ind_int - gamma_int).flatten()
    edge_number = 2*n_x * (n_y-1) + 2*(n_x-1)*n_y
    capacities = np.max(gamma_int) * np.ones(edge_number)
    start_nodes = []
    end_nodes = []
    costs = []

    for i in range(n_x):
        for j in range(n_y-1):
            node1 = i*n_x+j
            node2 = i*n_x+(j+1)
            cost = d_y[j, j+1]
            # connect in both directions
            start_nodes.append(node1)
            end_nodes.append(node2)
            costs.append(cost)

            start_nodes.append(node2)
            end_nodes.append(node1)
            costs.append(cost)
    for i in range(n_x-1):
        for j in range(n_y):
            node1 = i*n_y+j
            node2 = (i+1)*n_y+j
            cost = d_x[i, i+1]
            # connect in both directions
            start_nodes.append(node1)
            end_nodes.append(node2)
            costs.append(cost)

            start_nodes.append(node2)
            end_nodes.append(node1)
            costs.append(cost)

    res = min_cost_flow_ortools(start_nodes, end_nodes, costs, capacities, supplies, max_flow=False, return_flow=False)
    return res

def td_with_pivot(c_x, d_y, gamma_int, ind_int):
    """ This works only if y was a sorted array before computing d_y"""
    n_x, n_y = gamma_int.shape
    I, J = np.where(gamma_int>0)
    n = len(I)
    supplies = np.empty(n_x*n_y+n, dtype=int)
    supplies[:n_x*n_y] = ind_int.flatten()
    supplies[n_x*n_y:] = -gamma_int[I, J]
    
    start_nodes = []
    end_nodes   = []
    costs       = []

    for k in range(n):
        for i in range(n_x):
            node1 = i*n_y + J[k] 
            node2 = n_x*n_y + k
            cost = c_x[I[k], i]
            start_nodes.append(node1)
            end_nodes.append(node2)
            costs.append(cost)

    for i in range(n_x):
        for j in range(n_y-1):
            node1 = i*n_y+j
            node2 = i*n_y+(j+1)
            cost = d_y[j, j+1]

            # connect in both directions
            start_nodes.append(node1)
            end_nodes.append(node2)
            costs.append(cost)

            start_nodes.append(node2)
            end_nodes.append(node1)
            costs.append(cost)

    capacities = np.sum(gamma_int) * np.ones_like(costs, dtype=int) 

    res = min_cost_flow_ortools(start_nodes, end_nodes, costs, capacities, supplies, max_flow=False, return_flow=False)
    return res

def mtcor(x, y, p=1, d_x='euclidean', d_y='euclidean', solver=pot_solver, **solver_args):
    """
    Compute the marginal transport correlation. It is here assumed that x is categorical.
    Parameters
    ----------
    x : array_like
        needs to be categorical
    y : array_like
    p : float
    gamma : (n_x, n_y) matrix
    d_y : distance function (needs to be at least symmetric)
    solver : optimal transport solver
    solver_args : additional arguments for the optimal transport solver

    Returns
    -------
    transport correlation : float

    Reference
    --------
    [1] Transport Dependency: Optimal Transport Based Dependency Measures, Thomas Giacomo Nies, Thomas Staudt and Axel Munk, 2021
    """
    x_cat = np.unique(x)
    n_x = len(x_cat) 
    n = len(y)

    total_mass= n**2
    gamma_int = (x_cat[:, np.newaxis] == x) * n
    gamma_int = np.asarray(gamma_int, dtype=int)

    # build matrix mu_otimes_nu 
    mu_int, nu_int = marginals(gamma_int)
    mu_int, nu_int = mu_int//n, nu_int//n

    gamma = gamma_int/total_mass
    mu, nu = marginals(gamma)
    d_y = compute_cost_matrix(y, d_y)
    pdiam_y = diameter(d_y, nu, p)
    c_y = d_y**p / pdiam_y

    mtd = np.sum(np.array([solver(mu_int[i]*nu_int, gamma_int[i, :], c_y, **solver_args)
        for i in range(n_x)]))
    res = (mtd / total_mass)**(1/p) 
    return res

def marginals(gamma):
    """ Computes histogram of marginals given a histogram on product space."""
    return np.sum(gamma, axis=1), np.sum(gamma, axis=0) 

def diameter(d, mu, p):
    """Computes the p-diameter given a distance matrix.""" 
    return np.einsum("i, ij, j", mu, d**p, mu)

def compute_cost_matrix(points, d_function):
    """ Computes pairwise distance between points."""
    n = len(points)
    if len(points.shape) < 2:
        points = points.reshape(n, 1)
    d = squareform(pdist(points, d_function))
    return d 

def alpha_star(x, y, d_x="euclidean", d_y="euclidean", p=1):
    """ Computes alpha star"""
    d_x = distance_matrix(x, d_x)
    d_y = distance_matrix(y, d_y)
    mu = np.ones(len(x))/len(x)
    nu = np.ones(len(y))/len(y)

    pdiam_x = diameter(d_x, mu, p)
    pdiam_y = diameter(d_y, nu, p)

    alpha = pdiam_y/pdiam_x
    return alpha
