from ortools.graph.python import min_cost_flow
import numpy as np

def ot_solver(a, b, c):
    a = np.array(a)
    b = np.array(b)
    c = np.array(c)
    n = len(a)
    m = len(b)
    supplies = np.hstack([a, -b])
    costs = c.flatten()
    start_nodes, end_nodes = np.meshgrid(np.arange(n), np.arange(m), indexing="ij")
    start_nodes = start_nodes.flatten()
    end_nodes = end_nodes.flatten() 
    end_nodes = end_nodes + n
    capacities = a[start_nodes]
    res = min_cost_flow_ortools(start_nodes, end_nodes, costs, capacities, supplies, return_flow=False)
    return res

def min_cost_flow_ortools(start_nodes, end_nodes, costs, capacities,
        supplies, max_flow=False, return_flow=True):
    """Solve the min cost flow problem using the ortools-solver after converting
    the costs, capacity and supply values to integers."""
    int_costs, scaling_factor_for_cost = scale_to_int(costs)
    v, scaling_factor_for_supplies = scale_to_int(np.hstack([supplies, capacities]))
    int_supplies = v[:len(supplies)]
    int_capacities= v[len(supplies):]

    # Instantiate a SimpleMinCostFlow solver.
    smcf = min_cost_flow.SimpleMinCostFlow()

    # smcf.add_arcs_with_capacity_and_unit_cost(start_nodes, end_nodes, capacities, costs)
    # setting edges
    for k in range(len(start_nodes)):
        start_node = int(start_nodes[k])
        end_node = int(end_nodes[k])
        capacity = int(int_capacities[k])
        cost = int(int_costs[k])
        # print(start_node, end_node, capacity, cost)
        smcf.add_arcs_with_capacity_and_unit_cost(start_node, end_node, capacity, cost)

    # setting supply
    for k in range(len(int_supplies)):
        node = k 
        supply = int(int_supplies[k])
        # print(node, supply)
        smcf.set_node_supply(node, supply)

    # solve network flow problem
    if max_flow == True:
        check = smcf.solve_max_flow_with_min_cost()
    else:
        check =  smcf.solve()

    if check != smcf.OPTIMAL:
        raise Exception('There was an issue with the min cost flow input.')

    if return_flow:
        # get flow solution
        flows = []
        for k in range(len(start_nodes)):
            flow = smcf.flow(k)
            flows.append(flow)
        res = np.array(flows) * scaling_factor_for_supplies
    else:
        res = smcf.optimal_cost() * scaling_factor_for_cost * scaling_factor_for_supplies
    return res

def isinteger(x):
    return np.all(np.equal(np.mod(x, 1), 0))

def scale_to_int(cost, max_int=2**20-2):
    """ Convert float cost to numpy int32 by rescaling to a certain large maximal integer value and rounding (but only if necessary)."""
    cost = np.array(cost) # convert to numpy
    if isinteger(cost):
        scaling_factor = 1
        int_cost = cost
    else:
        max_abs_cost = np.max(np.abs(cost))
        scaling_factor = (max_int/max_abs_cost)
        int_cost = cost*scaling_factor

        int_cost = np.rint(int_cost) # covnert to integers

    int_cost = int_cost.astype(np.int32).tolist()
    return int_cost, 1/scaling_factor


if __name__ == "__main__":
    # small test example
    supplies = [20, 0, 0, -5, -15]

    start_nodes = [0, 0, 1, 1, 1, 2, 2, 3, 4]
    end_nodes = [1, 2, 2, 3, 4, 3, 4, 4, 2]

    capacities = [15, 8, 20, 4, 10, 15, 4, 20, 5]
    costs = [4, 4, 2, 2, 6, 1, 3, 2, 3]

    # Define an array of supplies at each node.

    flows = min_cost_flow_ortools(start_nodes, end_nodes, costs, capacities, supplies, max_flow=False, return_flow=True)
    print("This should be 150: ", np.sum(flows*costs))
    a = np.array([1, 1, 1, 2])
    b = np.array([1, 2, 2])
    c = np.random.random(size=(len(a), len(b)))
    import ot
    print(ot_solver(b, a, c), ot.emd2(a, b, c))
