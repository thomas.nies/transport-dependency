from transport_dependency.independence_test import __permutation_test_non_random
from multiprocessing import Pool
import numpy as np

def estimate_power_of_permutation_test(coef, data_gen_func, level, m, n_power, n_cors=None, print_result_of_each_permutation=False):
    """ 
    Estimate the power of a permutation test.

    Parameters
    ----------
    coef : function
         A dependency coefficient to evaluate the dependency between x and y. By default the
         transport correlation is used
    data_gen_func : function 
        A function that returns a pair x, y of data points evey time it is called 
    level: float 
         The level of the test
    m    : int
        Number of resamples for permutation test
    n_power : int
        Number of resamples for power estimation
    print_result_of_each_permutation : bool
        if True, at each iteration the test result is printed
    
    Returns
    -------
    beta : float
        The esimated power
    """
    # need to fix all permutations in advance to ensure the code to be reproduceble under a 
    # given seed
    xys = [data_gen_func() for i in range(n_power)]
    n = len(xys[0][0])
    perms = np.array([[np.random.permutation(n) for _ in range(m)] for j in range(n_power)])
    I = np.arange(n_power)
    global _ith_rep_of_test
    def _ith_rep_of_test(i):
        perm = perms[i, :, :]
        x, y = xys[i]
        test = __permutation_test_non_random(x, y,
                perm, coef=coef,
                level=level, m=m, exact_level=True,
                use_multiprocessing=False)
        if print_result_of_each_permutation:
            print("executed test number ", i)

        return test["test_res"]

    with Pool(n_cors) as p:
        tests = p.map(_ith_rep_of_test, I)

    beta = np.sum(tests)/n_power
    return beta

