"""This module contains several, potentially usefull functions to generate different types of noisy datasets"""
import numpy as np

def linear(n):
    x = np.random.uniform(size=n)
    y = x
    return x, y

def linear_10d(n):
    x = np.random.uniform(size=(n, d))
    y = x
    return x, y
def one_high_dim_marginal(n):
    d = 5
    x = np.random.uniform(size=(n,d))
    y = np.sum(x, axis=1)
    return x, y

def high_dimentional(n):
    d = 5
    x = np.random.uniform(size=(n,d))
    y = x
    return x, y
def parabolic(n):
    x = np.random.uniform(low=-1, high=1.0, size=n)
    y = (x)**2
    return x, y

def square(n):
    x, y = np.random.uniform(low=-1.0, high=1.0, size=(2,n))
    radians = np.pi/4
    xx = x * np.cos(radians) + y * np.sin(radians)
    yy = -x * np.sin(radians) + y * np.cos(radians)
    return xx/np.sqrt(2), yy/np.sqrt(2)

def cubic(n):
    x = np.random.uniform(low=-1, high=1, size=n)
    a = 1/2
    k = 3/(3-a**2)
    y = k*x*(x-a)*(x+a)
    return x, y 

def independent(n):
    x = np.random.uniform(size=n)
    y = np.random.uniform(size=n)
    return x, y
def independent_density(ax):
    x = [ 0, 1]
    y_up = [1, 1]
    y_down = [0, 0]
    ax.fill_between(x, y_up, y_down, alpha=0.05, color="tab:blue") 

def sinusoidal(n):
    x = np.random.uniform(low=0, high=3*np.pi, size=n)
    # this has period 1 and a half   
    y = 3 * np.sin(x)   
    return x, y

def sin4(n):
    x = np.random.uniform(low=-1, high=1, size=n)
    # this has period 1 and a half   
    y = np.sin(x*4*np.pi)  
    return x, y

def cos(n):
    x = np.random.uniform(low=-1, high=1, size=n)
    # this has period 1 and a half   
    y = np.cos(x*3)  
    return x, y

def one_period_sin(n):
    x = np.random.uniform(low=0, high=2*np.pi, size=n)
    # this has period 1 and a half   
    y = 3*np.sin(x)   
    return x, y

def exponential(n):
    x = np.random.uniform(size=n)
    y = np.exp(x)
    return x, y

def circle(n):
    t = np.random.uniform(size=n)
#     t = np.sort(t)
    x= np.cos(2*np.pi * t)
    y= np.sin(2*np.pi * t)
    return x, y

def brezel(n):
    t = np.random.uniform(size=n)
#     t = np.sort(t)
    y = np.cos(((t-0.5)*2)*2*np.pi)          
    x = np.sin(((t-0.5)*1.5)*2*np.pi) 
    return x, y

def discontinuous(n):
    x = np.random.uniform(size=n)
    y = x.copy()
    y[np.where(x>0.5)] = (-x + 1.5)[np.where(x>0.5)]
    return x, y

def ziczag_function(x, zacs):
    y1 = zacs*x - np.floor(zacs*x)
    y2 = -zacs*x + np.floor(zacs*x) + 1
    y =  np.where(np.floor(zacs*x)%2 == 0, y1, y2) 
    return y

def ziczag3(n):
    zacs = 3
    x = np.random.uniform(size=n)
    y1 = zacs*x - np.floor(zacs*x)
    y2 = -zacs*x + np.ceil(zacs*x)
    y =  np.where(np.floor(zacs*x)%2 == 0, y1, y2) 
    return x, y

def ziczag5(n):
    zacs = 5
    x = np.random.uniform(size=n)
    y1 = zacs*x - np.floor(zacs*x)
    y2 = -zacs*x + np.ceil(zacs*x)
    y =  np.where(np.floor(zacs*x)%2 == 0, y1, y2) 
    return x, y
        
def X(n):
    t = np.random.uniform(size=n)
    which = np.random.choice(a=[False, True], size=n, p=[1/2, 1/2])
    x1 = t[which]
    x2 = t[np.logical_not(which)]
    y1 = x1
    y2 = -x2 + 1
    x = np.concatenate([x1, x2])
    y = np.concatenate([y1, y2])
    return x, y

def unif_on_sfere(n, d):
    points = np.random.multivariate_normal(np.zeros(d), np.eye(d), size=n)
    # select points within unit ball
    norms = np.linalg.norm(points, axis=1).reshape((n, 1))
    points_on_sfere = points / norms
    return points_on_sfere

def data_from_sfere(n, n_x, n_y):
    d = n_x + n_y # sum of marginal dimensions
    points = unif_on_sfere(n, d)
    x, y = points[:, :n_x].reshape((-1, n_x)), points[:, n_x:].reshape((-1, n_y))
    return x, y

def sfere11(n):
    x, y = data_from_sfere(n, 1, 1)
    return x, y
def sfere21(n):
    x, y = data_from_sfere(n, 2, 1)
    return x, y
def sfere51(n):
    x, y = data_from_sfere(n, 5, 1)
    return x, y
def sfere501(n):
    x, y = data_from_sfere(n, 50, 1)
    return x, y
def sfere22(n):
    x, y = data_from_sfere(n, 2, 2)
    return x, y
def sfere55(n):
    x, y = data_from_sfere(n, 5, 5)
    return x, y
def sfere500500(n):
    x, y = data_from_sfere(n, 500, 500)
    return x, y
def sfereindependent(n):
    x, y = sfere55(n)
    # m random permutations of y. 
    perm_y = np.random.permutation(y)
    return x, perm_y

def lin11(n):
    x = np.random.random(n)
    y = x
    return x, y
def lin21(n):
    x = np.random.random(size=(n, 2))
    y = x[:, 0].reshape((n, 1))
    return x, y
def lin22(n):
    x = np.random.random(size=(n, 2))
    y = x
    return x, y
def lin55(n):
    x = np.random.random(size=(n, 5))
    y = x
    return x, y

### Some functions to plot the shape of the data
def square_density(ax, alpha):
    # in this case alpha has no effect since the density of an area is plotted
    x = [-1, 0, 1]
    y_up = [0, 1, 0]
    y_down = [0, -1, 0]
    ax.fill_between(x, y_up, y_down, alpha=0.05, color="tab:blue") 

def X_density(ax, alpha):
    x_1 = [0, 1]
    y_1 = [0, 1]
    y_2 = [1, 0]
    ax.plot(x_1, y_1, alpha=alpha, color="tab:blue")
    ax.plot(x_1, y_2, alpha=alpha, color="tab:blue")

def disk_density(ax):
    x = [-1, 0, 1]
    y_up = [0, 1, 0]
    y_down = [0, -1, 0]
    ax.fill_between(x, y_up, y_down, alpha=0.05, color="tab:blue") 


def discontinuous_density(ax, alpha):
    x_all, y_all = discontinuous(2000)
    less = np.where(x_all>1/2)
    more = np.where(x_all<1/2)
    x_1, y_1 = x_all[less], y_all[less]
    x_2, y_2 = x_all[more], y_all[more]
    ax.plot(x_1, y_1, alpha=alpha, color="tab:blue")
    ax.plot(x_2, y_2, alpha=alpha, color="tab:blue")

def brezel_density(ax, alpha):
    n = 2000
    t = np.random.uniform(size=n)
    t = np.sort(t)
    y = np.cos(((t-0.5)*2)*2*np.pi)          
    x = np.sin(((t-0.5)*1.5)*2*np.pi) 
    ax.plot(x, y, alpha=alpha, color="tab:blue")

def circle_density(ax, alpha):
    n = 1000
    t = np.random.uniform(size=n)
    t = np.sort(t)
    x= np.cos(2*np.pi * t)
    y= np.sin(2*np.pi * t)
    ax.plot(x, y, alpha=alpha, color="tab:blue")

def plot_density(ax, alpha, data_type):
    if data_type.__name__== "circle":
        circle_density(ax, alpha)
    elif data_type.__name__== "X":
        X_density(ax, alpha)
    elif data_type.__name__== "brezel":
        brezel_density(ax, alpha)
    elif data_type.__name__== "discontinuous":
        discontinuous_density(ax, alpha)
    elif data_type.__name__== "square":
        square_density(ax, alpha)
    elif data_type.__name__== "independent":
        return
    else:
        n = 2000
        x, y = data_type(n)
        x_pos = np.argsort(x)
        x, y = x[x_pos], y[x_pos]
        ax.plot(x, y, alpha=alpha, color="tab:blue")

if __name__ == "__main__":
    import matplotlib.pyplot as plt

    def plot_data(ax, data_gen, n):
        x, y = data_gen(n)
        ax.scatter(x, y, s=0.4)
        ax.set_title(f'{data_gen.__name__}' ,fontsize=10)
        ax.set_frame_on(False)
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        ax.plot(x, y, ',')
        ax.set_xticks([])
        ax.set_yticks([])

    n = 50
    list_to_plot = [ziczag5, 
        square,
        circle,
        X,
        brezel,
        discontinuous
        ]
    fig, ax = plt.subplots(2, 3, figsize=(8, 4))
    for ij in range(6):
        i, j = np.unravel_index(ij, shape=(2, 3))
        plot_data(ax[i, j], list_to_plot[ij] , n)
        plot_density(ax[i, j], 0.2, list_to_plot[ij])
    plt.show()
