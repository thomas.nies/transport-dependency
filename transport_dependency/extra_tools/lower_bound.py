import cvxpy as cp
import numpy as np
from scipy.spatial.distance import pdist, squareform, cdist
import transport_dependency as td
# from ot_tools import td

def distance_matrix(points, distance="euclidean"):
    n = points.shape[0]
    if len(points.shape) < 2:
        points = points.reshape(n, 1)
    d = squareform(pdist(points, distance))
    return d 

def lipschtiz_regression(x, y, p, alpha, d_x="euclidean"):
    n = len(x)
    # constrans |f_i - f_j|\leq \alpha d_(x_i, x_j)  
    d_mat = distance_matrix(x, d_x) 
    i = np.arange(n)
    j = np.arange(n)
    f_index_constrain = np.zeros((n*(n-1), n))
    dist = np.zeros(n*(n-1))
    for i in np.arange(start=0, stop=n-1):
        j = i+1
        while j<n:
            f_index_constrain[i*n + j, i] = 1
            f_index_constrain[i*n + j, j] = -1
            dist[i*n + j] = d_mat[i, j]
            j = j + 1
    
    # Construct the problem.
    y = y.flatten()
    f = cp.Variable(n)
    objective = cp.Minimize(cp.sum(cp.power(cp.abs(f - y), p)))
    constraints = [cp.abs(f_index_constrain @ f) <= alpha * dist]
    prob = cp.Problem(objective, constraints)
    
    result = prob.solve()
    return f.value

def risk(f, y, p):
    return np.sum(np.abs(f-y)**p)/len(y)

def risk_ind(f, y, p):
    return np.sum(np.abs(f.reshape((-1, 1))-y.reshape((1, -1)))**p)/len(y)**2

def diameter(d, mu, p):
    """Computes the p-diameter given a distance matrix.""" 
    return np.einsum("i, ij, j", mu, d**p, mu)

def lb(x, y, p=1, alpha=None, scale=True):
    """Computes the lower bound of the transport dependency"""
    mu = np.ones(len(x))/len(x)
    nu = np.ones(len(y))/len(y)
    d_x = distance_matrix(x)
    d_y = distance_matrix(y)
    pdiam_x = diameter(d_x, mu, p)
    pdiam_y = diameter(d_y, nu, p)
    if alpha is None: # determine alpha_star
        alpha = pdiam_y/pdiam_x
    f = lipschtiz_regression(x, y, p, alpha)
    rg = risk(f, y.flatten(), p)
    ri = risk_ind(f, y, p)
    lbound = np.abs(ri**(1/p) - rg**(1/p))
    if scale:
        return (lbound/pdiam_y**(1/p))
    else:
        return lbound
