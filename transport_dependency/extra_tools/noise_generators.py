"""This module contains several, potentially usefull functions to generate different types of noisy datasets"""
import numpy as np

def independent_noise(n, e, data_gen_func):
    """ Output distributes as (1-e)gamma + e*muotimesnu given data sampled
    from gamma through the function data_gen_func""" 

    which = np.random.choice(a=[False, True], size=n, p=[1-e, e]) # which data comes from the 
                                                                  # independent coupling
    n_ind = np.count_nonzero(which)   # number of data points from independent coupling.                           
    x_all, y_all = data_gen_func(n+n_ind) # extra data is sampled
    sigma = np.random.permutation(n_ind + n)
    y_all = y_all[sigma]              # any possible ordering is removed
    x_all = x_all[sigma]
    x = x_all[:n]                         # extra entries of x are discharged
    y = y_all[:n]
    y_ind = y_all[n:]
    y[which] = y_ind              # some entries of y are replaced throught the independent
    return x, y

def gaussian_noise_y_axis(n, e, data_gen_func):
    """ Output distributes as X, Y + R with R random normal noise.""" 
    x, y = data_gen_func(n)
    y= y + np.random.normal(loc=0, scale=e, size=y.shape)
    return x, y

def gaussian_noise_both_axis(n, e, data_gen_func):
    """ Output distributes as X + R_1, Y + R_2 with R_1 and R_2 random normal noise.""" 
    x, y = data_gen_func(n)
    x= x + np.random.normal(loc=0, scale=e, size=x.shape)
    y= y + np.random.normal(loc=0, scale=e, size=y.shape)
    return x, y 

