import subprocess
from multiprocessing import Pool

def f(ij):
    i = ij[0]
    j = ij[1]
    subprocess.run(["python3", "command_plot.py", str(i), str(j)])
l = []
for i in range(9):
    for j in [0, 2]:
        l.append([i,j])
print(l)
with Pool(1) as p:
    p.map(f, l)

