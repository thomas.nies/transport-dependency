import numpy as np
import matplotlib.colors as mcolors
import coefficients

def plot_power(ax, eps_lab_test, coefficient_list, beta, eps_lab, alpha, xlabel):
#    tf = np.where( np.arange(len(eps_lab_test))%2 == 0 ) # alternating ture and false
    n_coef = len(coefficient_list)
    for i in range(n_coef):
#_        ax.plot(eps_lab_test[tf], beta[:, i][tf], label=coefficients.dic_of_names[coefficient_list[i]])
        ax.plot(eps_lab_test, beta[:, i], label=coefficients.dic_of_names[coefficient_list[i]])
        ax.set_xticks(eps_lab)
    ax.axhline(y=alpha, color='gray', linestyle="dashed", alpha=0.5)
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1.05))
    ax.set_xlabel(xlabel)
    ax.set_ylabel("power")
    ax.legend()
    ax.xaxis.grid()
    ax.yaxis.grid()

def coefficient_box_plot(ax, eps_index, eps_lab, all_coef_value, mw, coefficient_list, ylim_boxplots, noticks=False):
    eps = eps_lab[eps_index]
    coef_value = all_coef_value[eps_index, :, :]

    point_instead_of_box = np.max(coef_value, axis=0)-np.min(coef_value, axis=0)<mw
    n_coef = len(coefficient_list)

    # prepare colors
    colors = mcolors.TABLEAU_COLORS
    colors = np.array(list(colors)[:n_coef])
    colors_for_box = colors[~point_instead_of_box]
    colors_for_points = colors[point_instead_of_box]

    # create boxplots
    box_positions = np.arange(n_coef)[~point_instead_of_box]
    if len(box_positions)>0:
        bplot = ax.boxplot(coef_value[:, ~point_instead_of_box], patch_artist=True, positions=box_positions)
        for patch, color in zip(bplot['boxes'], colors_for_box):
            patch.set_facecolor(color)

    # plot points when boxplot is degenerate
    values = np.mean(coef_value, axis=0)[point_instead_of_box]
    x_point_positions = np.arange(n_coef)[point_instead_of_box]
    for i in range(len(values)):
        ax.scatter(x_point_positions[i], values[i], color=colors_for_points[i], edgecolors="black", zorder=3)
    ax.set_ylim(ylim_boxplots)
    if noticks:
        ax.set_yticks([])

    # add ticks on x axis (one for each coefficient)
    ax.set_xticks([i for i in range(n_coef)])
    names = [coefficients.dic_of_names[coefficient_list[i]] for i in range(n_coef)]
    ax.set_xticklabels(names, rotation=45)

    # add gray lines 
    ax.yaxis.grid(zorder=0)
