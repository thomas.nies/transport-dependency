import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.colors as mcolors
mpl.rcParams['boxplot.medianprops.color'] = 'black'
import matplotlib.gridspec as gridspec
from multiprocessing import Pool

from transport_dependency.extra_tools import data_generators
from transport_dependency.extra_tools import noise_generators
from transport_dependency.extra_tools.estimate_power import estimate_power_of_permutation_test

# local module is imported here
import coefficients
from utils import plot_power, coefficient_box_plot

use_already_computed_data = True

# parameters for data generation
seed = 0
np.random.seed(seed)
noise_lab = np.linspace(start=0, stop=1, num=31)
n = 50 # number of data points
m_list = np.array([9, 9]) # for estimation of quantile in test statistics
n_power = 1000 # for power estimation
level = 0.1
noise_type = noise_generators.independent_noise

data_type_list = [data_generators.sfere11,
        data_generators.sfere21,
        data_generators.sfere22,
        data_generators.sfere55,
        data_generators.lin11,
        data_generators.lin21,
        data_generators.lin22,
        data_generators.lin55
        ]
coefficient_list = [coefficients.tcor, coefficients.dcor] 

data_file_name_power = "./data/power_values_for_high_dims_plot.npy"
# compute power 
if use_already_computed_data and os.path.isfile(data_file_name_power):
    beta = np.load(data_file_name_power)
else:
    n_data_types = len(data_type_list)
    n_eps_pow = len(noise_lab)
    n_coef = len(coefficient_list)
    beta = np.empty((n_data_types, n_eps_pow, n_coef))
    for i in range(n_data_types):
        data_type = data_type_list[i] 
        for j in range(n_eps_pow):
            eps = noise_lab[j]
            print("noise level:", eps)
            # create a function that generates the data for the power computation
            def data_gen_func():
                x, y = noise_type(n, eps, data_type)
                return x, y
            # the function needs an informative name, to be saved uniquely in the dictionary
            for k in range(n_coef):
                coef = coefficient_list[k]
                m = m_list[k]
                beta[i, j, k] = estimate_power_of_permutation_test(coef, data_gen_func, level, m, n_power)
                print(beta[i, j, k])
        np.save(data_file_name_power, beta)

level = 0.1

figsize = (7, 6)
fig, ax = plt.subplots(nrows=2, ncols=2, figsize=figsize)
def plot_powers(ax, i):
    ax.plot(noise_lab, beta[i+4, :, 0], color="tab:blue", linestyle="--", label=r"linear, $\rho_*$")
    ax.plot(noise_lab, beta[i+4, :, 1], color="tab:orange", linestyle="--", label="linear, dcor")
    ax.plot(noise_lab, beta[i, :, 0], color="tab:blue", label=r"sphere, $\rho_*$")
    ax.plot(noise_lab, beta[i, :, 1], color="tab:orange", label="sphere, dcor")
for i in range(2):
    for j in range(2):
        plot_powers(ax[i, j], 2*i+j)
        ax[i, j].axhline(y=level, color='gray', linestyle="dashed", alpha=0.5)
ax[1, 0].set_xlabel(r'$\epsilon$')
ax[1, 1].set_xlabel(r'$\epsilon$')
ax[0, 0].set_ylabel("power")
ax[1, 0].set_ylabel("power")
ax[0, 0].set_xticklabels([])
ax[0, 1].set_xticklabels([])

ax[0, 0].set_title(r"$(q, r)=(1, 1)$")
ax[0, 1].set_title(r"$(q, r)=(2, 1)$")
ax[1, 0].set_title(r"$(q, r)=(2, 2)$")
ax[1, 1].set_title(r"$(q, r)=(5, 5)$")
ax[1, 0].legend()
plt.savefig("./figures/high_dim_tests.pdf")
plt.show()
