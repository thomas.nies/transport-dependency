import numpy as np
import os
import matplotlib.pyplot as plt

from transport_dependency.extra_tools import data_generators
from transport_dependency.extra_tools import noise_generators

# local module is imported here
import coefficients

import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib as mpl
mpl.rcParams['boxplot.medianprops.color'] = 'black'

use_already_computed_data = True
data_file_name = "./data/geometric-data-2.npy"
plot_file_name = "./figures/geometric-data-2.pdf"

# parameters for data generation
seed = 0
data_type_list = [data_generators.X, data_generators.circle, data_generators.sin4, data_generators.brezel,  data_generators.independent]
n = 50  # number of data points
coefficient_list = [coefficients.tcor_star, coefficients.tcor3, coefficients.dcor, coefficients.pearson, coefficients.spearman, coefficients.mice] 

# parameters for comparision
n_repetitions = 100 # number of resamples for each noise level

if use_already_computed_data and os.path.isfile(data_file_name):
    coef_values = np.load(data_file_name)
else:
    np.random.seed(seed)
    n_coef = len(coefficient_list)
    
    def all_dependency_measures(x, y):
        values = [f(x, y) for f in coefficient_list]
        return np.array(values)
    
    n_coef = len(coefficient_list)
    coef_values = np.empty((len(data_type_list), n_repetitions, n_coef)) 
    for i in range(len(data_type_list)): 
        data_gen = data_type_list[i]
        for k in range(n_repetitions):
            print("rep: " , k , ", data: " , data_gen.__name__)
            x, y = data_gen(n)
            coef_values[i, k, :] = all_dependency_measures(x, y)
    np.save(data_file_name, coef_values)

#####  Plot

# parameters for plot
mw = 0.1 # minimal length of wiskers before replacing them with a dot
s = 1.0 # size of points in scatterplot
ylim = (-0.05, 1.05)
wspace = 0.1
hspace = 0.1
height_ratios = [1, 1]
figsize = (10, (10/5)*np.sum(height_ratios))

def coefficient_box_plot(ax, data_index, noticks=False):
    data = data_type_list[data_index]
    coef_value = coef_values[data_index, :, :]

    point_instead_of_box = np.max(coef_value, axis=0)-np.min(coef_value, axis=0)<mw
    n_coef = len(coefficient_list)

    # prepare colors
    colors = mcolors.TABLEAU_COLORS
    colors = np.array(list(colors)[:n_coef])
    colors_for_box = colors[~point_instead_of_box]
    colors_for_points = colors[point_instead_of_box]

    # create boxplots
    box_positions = np.arange(n_coef)[~point_instead_of_box]
    if len(box_positions)>0:
        bplot = ax.boxplot(coef_value[:, ~point_instead_of_box], patch_artist=True, positions=box_positions)
        for patch, color in zip(bplot['boxes'], colors_for_box):
            patch.set_facecolor(color)

    # plot points when boxplot is degenerate
    values = np.mean(coef_value, axis=0)[point_instead_of_box]
    x_point_positions = np.arange(n_coef)[point_instead_of_box]
    for i in range(len(values)):
        ax.scatter(x_point_positions[i], values[i], color=colors_for_points[i], edgecolors="black", zorder=2)
    ax.set_ylim(ylim)
    if noticks:
        ax.set_yticks([])

    # add ticks on x axis (one for each coefficient)
    ax.set_xticks([i for i in range(n_coef)])
    names = [' ' + coefficients.dic_of_names[coefficient_list[i]] for i in range(n_coef)]

    ax.set_xticklabels(names, rotation=45)
    ax.yaxis.grid()

def plot_data(ax, data):
    x, y = data(n) 
    ax.axis("equal")
    ax.scatter(x, y, s=s)
    ax.set_xticks([])
    ax.set_yticks([])
    # Plotting lines with denisty
    alpha = 0.2
    data_generators.plot_density(ax, alpha, data)

def plot():
    fig = plt.figure(figsize=figsize)
    gs = fig.add_gridspec(2, len(data_type_list), height_ratios=height_ratios)
    gs.update(wspace=wspace, hspace=hspace) # set the spacing between axes.
    for j in range(len(data_type_list)):
        ax0 = fig.add_subplot(gs[0, j])
        plot_data(ax0, data_type_list[j])
        ax1 = fig.add_subplot(gs[1, j])
        coefficient_box_plot(ax1, j)    
        if j!=0:
            ax1.set_yticklabels([])
        if j==0:
            ax1.set_ylabel("Coefficent value")

#     plt.tight_layout(pad=0.1)
    plt.savefig(plot_file_name)
    plt.show()

plot() 
