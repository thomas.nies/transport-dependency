""" Here we list all the coefficients we use in our simulation"""
import numpy as np
from transport_dependency import tcor
from transport_dependency.extra_tools.other_coeffs import kendall, pearson, spearman
import dcor as distance_correlation

# define MINE based coeffcients
from minepy import MINE

def mic(x, y):
    mine = MINE(alpha=0.6, c=15, est="mic_approx")
    mine.compute_score(x, y)
    return mine.mic()

    # The core approximation algorithm EQUICHARCLUMP speeds up the computation
    # of MICe. "The EQUICHARCLUMP parameter c controls the coarseness of the
    # discretization in the grid search phase 
    # and by default it is set to 5, providing good performance in most
    # settings" A quote from the paper: A practical tool for Maximal
    # Information Coefficient analysis alpha is chosen according to table 1 of
    # paper above. This is the optimal value for equitability when n \in [50,
    # 250]
def mice(x, y):
    mine = MINE(alpha=0.75, c=5, est="mic_e")
    mine.compute_score(x, y)
    return mine.mic()

def tic(x, y):
    mine = MINE(alpha=0.6, c=15, est="mic_e")
    mine.compute_score(x, y)
    return mine.tic()

# define some standard coefficients to be used in all the simulations
def tcor_star(x, y):
    return tcor(x, y)

def dcor(x, y):
    return distance_correlation.distance_correlation(x, y)

def udcor_sqr(x, y):
    return distance_correlation.distance_correlation(x, y)

def tcor_star_est(x, y):
    est_ind = True
    return np.minimum(tcor(x, y, est_ind=est_ind), 1)

def tcor3(x, y):
    alpha = 3
    return tcor(x, y, alpha=alpha) 

def tcor3_est(x, y):
    alpha = 3
    return tcor(x, y, alpha=alpha, est_ind=True) 

# different p
def tcor_p1r1(x, y):
    r = 1
    p = 1
    alpha = 1
    return tcor(x, y, alpha=alpha, r=r, p=p) 

def tcor_p2r1(x, y):
    r = 1
    p = 2
    alpha = 1
    return tcor(x, y, alpha=alpha, r=r, p=p) 
def tcor_p05r1(x, y):
    r = 1
    p = 0.5 
    alpha = 1
    return tcor(x, y, alpha=alpha, r=r, p=p) 

# r = 2
def tcor_p1r2(x, y):
    r = 2
    p = 1 
    alpha = 1
    return tcor(x, y, alpha=alpha, r=r, p=p) 
def tcor_p2r2(x, y):
    r = 2 
    p = 2 
    alpha = 1
    return tcor(x, y, alpha=alpha, r=r, p=p) 

def mtcor(x, y, rate=1/4, const=1): 
    """Marginal transport dependency estimator for real valued data"""
    alpha = const * len(x)**(rate) # rate at which alpha goes to infty.
    r = 1
    p = 2
    return tcor(x, y, alpha=alpha, r=1, p=1) 

dic_of_names = {tcor_star:r"$\rho_*$",
        tcor_star_est:r"$\hat\rho_*$",
        tcor3_est:r"$\hat\rho_3$",
        tcor3:r"$\rho_3$",
        dcor:r"$\mathrm{dcor}$",
        pearson:r"$\mathrm{cor}$",
        mice:r"$\mathrm{mic}$",
        kendall:r"ken",
        spearman:"$\mathrm{spe}$",
        tcor_p1r1:r"$p=1$",
        tcor_p2r1:r"$p=2$",
        tcor_p05r1:r"$p=\frac{1}{2}$",
        tcor_p2r2:r"$p=2, r=2$",
        tcor_p1r2:r"$p=1, r=2$"}
