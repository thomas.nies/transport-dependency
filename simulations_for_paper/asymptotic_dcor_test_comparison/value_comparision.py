import numpy as np
import transport_dependency as td
import dcor
import matplotlib.pyplot as plt

np.random.seed(0)
noise_level = 0.3 

sample_sizes = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
true_value = (1-noise_level)**2
dimensions = [[1, 1], [2, 2], [5, 5,], [10, 10]]
montecarlo_samples = 20

def value_comp():
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(15, 15))
    for i in range(len(dimensions)):
        p, q = dimensions[i]
        dcor_averages = []
        tcor_averages = []
        udcor_averages = []
        dcor_std = []
        tcor_std = []
        udcor_std = []
        for n in sample_sizes:
            dcor_vals = []
            tcor_vals = []
            udcor_vals = []
            print(p, q, n)
            for m in range(montecarlo_samples):
                which = np.random.choice(a=[False, True], size=n, p=[1-noise_level, noise_level])
                n_ind = np.count_nonzero(which)   # number of data points from independent coupling.
                x_all = np.random.random((n+n_ind, p)) # extra data is sampled
                y_all = x_all.copy()
                x = x_all[:n, :]      # extra entries of x are discharged
                y = y_all[:n, :]
                y_ind = y_all[n:, :]
                y[which] = y_ind   # some entries of y are replaced throught the independent
    
                dcor_vals.append(dcor.distance_correlation_sqr(x, y))
                tcor_vals.append(td.tcor(x, y)**2)
                udcor_vals.append(dcor.u_distance_correlation_sqr(x, y))
            dcor_averages.append(np.average(dcor_vals)) 
            tcor_averages.append(np.average(tcor_vals)) 
            udcor_averages.append(np.average(udcor_vals)) 
            dcor_std.append(np.std(dcor_vals))
            tcor_std.append(np.std(tcor_vals))
            udcor_std.append(np.std(udcor_vals))
        tcor_averages = np.array(tcor_averages)
        tcor_std = np.array(tcor_std)
        dcor_averages = np.array(dcor_averages)
        dcor_std = np.array(dcor_std)
        udcor_averages = np.array(udcor_averages)
        udcor_std = np.array(udcor_std)
        ax[i//2, i%2].set_title(str((p, q)))
        ax[i//2, i%2].plot(sample_sizes, tcor_averages, label="squared tcor", marker="o", color="tab:blue")
        ax[i//2, i%2].plot(sample_sizes, dcor_averages, label="squared dcor", marker="o", color="tab:orange")
        ax[i//2, i%2].plot(sample_sizes, udcor_averages, label="unbiased squared dcor", marker="o",
                color="tab:green")
        ax[i//2, i%2].fill_between(sample_sizes, tcor_averages-3*tcor_std, tcor_averages+3*tcor_std,
                color="tab:blue", alpha=0.1)
        ax[i//2, i%2].fill_between(sample_sizes, dcor_averages-3*dcor_std, dcor_averages+3*dcor_std,
                color="tab:orange", alpha=0.1)
        ax[i//2, i%2].fill_between(sample_sizes, udcor_averages-3*udcor_std, udcor_averages+3*udcor_std,
                color="tab:green", alpha=0.1)
        ax[i//2, i%2].grid(visible=True) 
        ax[i//2, i%2].legend() 
        ax[i//2, i%2].set_xlabel("n") 
        ax[i//2, i%2].set_ylabel("correlation value") 
        ax[i//2, i%2].plot(sample_sizes, true_value*np.ones_like(sample_sizes),
                color="black", label="True value")
    
    plt.savefig("compare_values.pdf") 
    plt.show()

def error_comp():
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(15, 15))
    for i in range(len(dimensions)):
        p, q = dimensions[i]
        dcor_error = []
        tcor_error = []
        udcor_error = []
        dcor_std = []
        tcor_std = []
        udcor_std = []
        for n in sample_sizes:
            dcor_vals = []
            tcor_vals = []
            udcor_vals = []
            print(p, q, n)
            for m in range(montecarlo_samples):
                which = np.random.choice(a=[False, True], size=n, p=[1-noise_level, noise_level])
                n_ind = np.count_nonzero(which)   # number of data points from independent coupling.
                x_all = np.random.random((n+n_ind, p)) # extra data is sampled
                y_all = x_all.copy()
                x = x_all[:n, :]      # extra entries of x are discharged
                y = y_all[:n, :]
                y_ind = y_all[n:, :]
                y[which] = y_ind   # some entries of y are replaced throught the independent
    
                dcor_vals.append(dcor.distance_correlation(x, y))
                tcor_vals.append(td.tcor(x, y))
                udcor_vals.append(np.maximum(dcor.u_distance_correlation_sqr(x, y), 0)**0.5)
            dcor_vals = np.array(dcor_vals)
            udcor_vals = np.array(udcor_vals)
            tcor_vals = np.array(tcor_vals)
            dcor_error.append(np.average((dcor_vals-noise_level)**2)) 
            tcor_error.append(np.average((tcor_vals-noise_level)**2))
            udcor_error.append(np.average((udcor_vals-noise_level)**2)) 
        ax[i//2, i%2].set_title(str((p, q)))
        ax[i//2, i%2].plot(sample_sizes, tcor_error, label="squared tcor", marker="o", color="tab:blue")
        ax[i//2, i%2].plot(sample_sizes, dcor_error, label="squared dcor", marker="o", color="tab:orange")
        ax[i//2, i%2].plot(sample_sizes, udcor_error, label="unbiased squared dcor", marker="o",
                color="tab:green")
        ax[i//2, i%2].grid(visible=True) 
        ax[i//2, i%2].legend() 
        ax[i//2, i%2].set_xlabel("n") 
        ax[i//2, i%2].set_ylabel("mean squared error") 
        ax[i//2, i%2].plot(sample_sizes, np.zeros_like(sample_sizes),
                color="black")
    
    plt.savefig("compare_errors.pdf") 
    plt.show()
error_comp()
