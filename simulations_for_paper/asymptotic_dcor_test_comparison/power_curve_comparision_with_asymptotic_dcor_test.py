import numpy as np
import transport_dependency as td
import dcor
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import norm

np.random.seed(0)

######### Code to reproduce example 2 and 3 in the paper:                               #####
######### ASYMPTOTIC DISTRIBUTIONS OF HIGH-DIMENSIONAL DISTANCE CORRELATION INFERENCE   ##### 

# here I define the 4 tests I uesd
def permutation_test_tcor(x, y, test_level):
    """ Permutation based independence test based on the biased sample distance correlation"""
    coef = td.tcor
    args = {"est_ind":True}
    # args = {}
    out = td.independence_test.permutation_test(x, y, coef=coef, level=test_level, use_multiprocessing=True, **args)
    test_res = out["test_res"]
    return test_res

def permutation_test_dcor(x, y, test_level):
    """ Permutation based independence test based on the biased sample distance correlation"""
    coef = dcor.distance_correlation
    out = td.independence_test.permutation_test(x, y, coef=coef, level=test_level, use_multiprocessing=True)
    test_res = out["test_res"]
    return test_res

def permutation_test_unbiased_dcor(x, y, test_level):
    """ Permutation based independence test based on the unbiased squared distance correlation"""
    coef = dcor.u_distance_correlation_sqr
    out = td.independence_test.permutation_test(x, y, coef=coef, level=test_level, use_multiprocessing=True)
    test_res = out["test_res"]
    return test_res

def unbiased_squared_dcor_independence_test(x, y, test_level):
    """ Test that was used in the paper (along with others which were added for comparison)"""
    value = dcor.u_distance_correlation_sqr(x, y)
    n = len(x)
    test_statistic = (n*(n-1)/2)**0.5*value
    q = norm.ppf(1-test_level)
    if test_statistic >= q:
        test_res = 1
    else:
        test_res = 0
    return test_res

def example_3_data_generator(n, d):
    I = np.arange(d) + 1
    Sigma = 0.5** np.abs(I[:, np.newaxis] - I[np.newaxis, :])
    x = np.random.multivariate_normal(mean=np.zeros(d), cov=Sigma, size=n)
    df = 4
    noise = np.random.standard_t(df, size=(n, d))
    y = 0.2 * (x + x**2) + noise 
    return x, y

def example_2_data_generator(n, d):
    I = np.arange(d) + 1
    Sigma = 0.5** np.abs(I[:, np.newaxis] - I[np.newaxis, :])
    x = np.random.multivariate_normal(mean=np.zeros(d), cov=Sigma, size=n)
    y = np.random.multivariate_normal(mean=np.zeros(d), cov=Sigma, size=n)
    return x, y

def data_generator_example_4(n, d):
    x = np.random.normal(size=(n, d))
    y = x**2
    return x, y

# Example 3 of the paper: independent data
def run_example(example_number):
    if example_number in [2, 3]:
        if example_number == 3:
            data_generator = example_3_data_generator
        elif example_number == 2:
            data_generator = example_2_data_generator
        
        fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(15, 15))
        # d_vals = [10, 50, 100, 300]
        d_vals = [2, 10, 50, 100]
        n_vals = [25, 75, 100, 150, 250]
        test_level = 0.05
        m = 100
        
        for i in range(4):
            d = d_vals[i]
            power_curve_perm_dcor_biased = []
            power_curve_perm_dcor_unbiased = []
            power_curve_asym = []
            power_curve_perm_tcor = []
            for n in n_vals:
                rejections_perm_bias = 0
                rejections_perm_unbias = 0
                rejections_asym = 0
                rejections_perm_tcor = 0
                for s in range(m):
                    print(d, n, s)
                    x, y = data_generator(n, d)
    
                    rejections_perm_bias += permutation_test_dcor(x, y, test_level=test_level)
                    rejections_perm_unbias += permutation_test_unbiased_dcor(x, y, test_level=test_level)
                    rejections_asym += unbiased_squared_dcor_independence_test(x, y, test_level=test_level)
                    rejections_perm_tcor += permutation_test_tcor(x, y, test_level=test_level)
    
                power_curve_perm_dcor_biased.append(rejections_perm_bias/m)
                power_curve_perm_dcor_unbiased.append(rejections_perm_unbias/m)
                power_curve_asym.append(rejections_asym/m)
                power_curve_perm_tcor.append(rejections_perm_tcor/m)
            ax[i//2, i%2].set_title("dimension: " + str(d))
            ax[i//2, i%2].plot(n_vals, power_curve_perm_tcor, label="tcor perm test", marker="o")
            ax[i//2, i%2].plot(n_vals, power_curve_perm_dcor_biased, label="dcor perm test", marker="o")
            ax[i//2, i%2].plot(n_vals, power_curve_perm_dcor_unbiased, label="u-sqr-dcor perm test", marker="o")
            ax[i//2, i%2].plot(n_vals, power_curve_asym, label="asymptotic test", marker="o")
            ax[i//2, i%2].legend()
            ax[i//2, i%2].set_xlabel("n")
            ax[i//2, i%2].set_ylabel("power")
            ax[i//2, i%2].grid(visible=True)
            ax[i//2, i%2].set_ylim([0, 1])
        plt.savefig("plot_for_example_" + str(example_number) + ".pdf")
        plt.show()
    elif example_number == 4:
        test_level = 0.05
        data = pd.DataFrame(columns=["n", "d", "dcor perm test", "u-sqr-dcor perm test", "asymptotic test", "tcor perm test"])
        dims = [6, 12, 16, 20, 22, 26]
        ns = [10, 40, 70, 100, 130, 170]
        m = 100
        for i in range(len(dims)):
            d = dims[i]
            n = ns[i]
            rejections_perm_bias = 0
            rejections_perm_unbias = 0
            rejections_asym = 0
            rejections_perm_tcor = 0
            for s in range(m):
                x, y = data_generator_example_4(n, d)
    
                rejections_perm_bias += permutation_test_dcor(x, y, test_level=test_level)/m
                rejections_perm_unbias += permutation_test_unbiased_dcor(x, y, test_level=test_level)/m
                rejections_asym += unbiased_squared_dcor_independence_test(x, y, test_level=test_level)/m
                rejections_perm_tcor += permutation_test_tcor(x, y, test_level=test_level)/m
            print("computing values for sample size: ", n, " and dimension: ", d)
            data.loc[i] = [n, d, rejections_perm_bias, rejections_perm_unbias, rejections_asym, rejections_perm_tcor]
        print(data)
        data.to_csv("example_4_data.csv")




run_example(2)
run_example(3)
run_example(4)
