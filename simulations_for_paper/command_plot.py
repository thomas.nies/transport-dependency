import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['boxplot.medianprops.color'] = 'black'
import matplotlib.gridspec as gridspec

from transport_dependency.extra_tools import data_generators
from transport_dependency.extra_tools import noise_generators
from transport_dependency.extra_tools.estimate_power import estimate_power_of_permutation_test

# local module is imported here
import coefficients
from utils import plot_power, coefficient_box_plot

use_already_computed_data = True
plot_file_name = "./figures/plot"+ str(sys.argv[2])+str(sys.argv[1])+".pdf"
data_file_name_coef = "./data/coef_values_for_plot"+ str(sys.argv[2])+str(sys.argv[1])+".npy"
data_file_name_power = "./data/power_values_for_plot"+ str(sys.argv[2])+str(sys.argv[1])+".npy"

# parameters for data generation
seed = 0

data_type_list = [data_generators.linear,
        data_generators.ziczag3,
        data_generators.ziczag5,
        data_generators.cubic,
        data_generators.discontinuous, 
        data_generators.X,
        data_generators.circle,
        data_generators.sin4,
        data_generators.brezel,
        data_generators.independent
        ]
noise_type_list = [noise_generators.independent_noise,
        noise_generators.gaussian_noise_y_axis,
        noise_generators.gaussian_noise_both_axis
        ]
data_type = data_type_list[int(sys.argv[1])]
noise_type = noise_type_list[int(sys.argv[2])]

n = 50  # number of data points
coefficient_list = [coefficients.tcor_star, coefficients.tcor3, coefficients.dcor, coefficients.pearson, coefficients.spearman, coefficients.mice] 
eps_lab = [0, 1/4, 2/4, 3/4, 1]

# parameters for comparision
n_repetitions = 100 # number of resamples for each noise level

# parameters for test
m_list = np.array([29, 29, 999, 999, 999, 999]) # for estimation of quantile in test statistics
n_power = 1000 # for power estimation
alpha = 0.1
noise_lab = np.linspace(start=0, stop=1, num=31)

# compute coefficient values
if use_already_computed_data and os.path.isfile(data_file_name_coef):
    coef_values = np.load(data_file_name_coef)
else:
    np.random.seed(seed)
    n_coef = len(coefficient_list)
    n_eps_lab = len(eps_lab)
    coef_values = np.empty((n_eps_lab, n_repetitions, n_coef)) 
    for i in range(n_eps_lab): 
        eps = eps_lab[i]
        for k in range(n_repetitions):
            print("rep: " , k , ", noise level: " , eps)
            for l in range(n_coef):
                coef = coefficient_list[l]
                x, y = noise_type(n, eps, data_type)
                coef_values[i, k, l] = coef(x, y)
    np.save(data_file_name_coef, coef_values)

# compute power 
if use_already_computed_data and os.path.isfile(data_file_name_power):
    beta = np.load(data_file_name_power)
else:
    np.random.seed(seed)
    n_eps_pow = len(noise_lab)
    n_coef = len(coefficient_list)
    beta = np.empty((n_eps_pow, n_coef))
    for i in range(n_eps_pow):
       eps = noise_lab[i]
       print("Noise: ", eps)
       # create a function that generates the data for the power computation
       def data_gen_func():
           x, y = noise_type(n, eps, data_type)
           return x, y
       # the function needs an informative name, to be saved uniquely in the dictionary
       for j in range(n_coef):
           coef = coefficient_list[j]
           m = m_list[j]
           beta[i, j] = estimate_power_of_permutation_test(coef, data_gen_func, alpha, m, n_power)
    np.save(data_file_name_power, beta)

#################### Now plotting begins

# parameters for plot
s = 1.3 # size of points in scatterplot
mw = 0.05 # minimal length of wiskers before replacing them with a dot
legend_loc = "upper right"
if noise_type==noise_generators.independent_noise and (data_type in [data_generators.linear, data_generators.ziczag3]):
    show_epsilon = True
else:
    show_epsilon = False # show a dashed line indicating epsilon
ylim_boxplots = (-0.05, 1.05)
hspaces = [0.18, 0.3]
wspace = 0.1
height_ratios = [0.95, hspaces[0], 1, hspaces[1], 1.1]
figsize = (10, (10/5) * np.sum(height_ratios))

# Plot

def plot_data(ax, eps):
    x, y = noise_type(n, eps, data_type)
    # x, y = data_gen(eps)
    ax.axis("equal")
    ax.scatter(x, y, s=s)
    ax.set_xticks([])
    ax.set_yticks([])
    # ax.set_xlim([0, 1])
    # ax.set_ylim([0, 1])
    if noise_type == noise_generators.independent_noise:
        if eps==1/4:
            ax.set_title(r'$\epsilon=\frac{1}{4}$')
        elif eps==1/2:
            ax.set_title(r'$\epsilon=\frac{1}{2}$')
        elif eps==3/4:
            ax.set_title(r'$\epsilon=\frac{3}{4}$')
        elif eps==1/3:
            ax.set_title(r'$\epsilon=\frac{1}{3}$')
        elif eps==2/3:
            ax.set_title(r'$\epsilon=\frac{2}{3}$')
        else:
            ax.set_title(r'$\epsilon={{{0}}}$'.format(np.round(eps, decimals=2)))
    else:
        if eps==1/4:
            ax.set_title(r'$\sigma=\frac{1}{4}$')
        elif eps==1/2:
            ax.set_title(r'$\sigma=\frac{1}{2}$')
        elif eps==3/4:
            ax.set_title(r'$\sigma=\frac{3}{4}$')
        elif eps==1/3:
            ax.set_title(r'$\sigma=\frac{1}{3}$')
        elif eps==2/3:
            ax.set_title(r'$\sigma=\frac{2}{3}$')
        else:
            ax.set_title(r'$\sigma={{{0}}}$'.format(np.round(eps, decimals=2)))

    # Plotting lines with denisty
    alpha = 0.4
    data_generators.plot_density(ax, alpha, data_type)

def plot(show_epsilon):
    fig = plt.figure(figsize=figsize)
    bottom = 0.06
    top = 0.96 
    left = 0.06
    right = 0.96
    gs = fig.add_gridspec(5, len(eps_lab), height_ratios=height_ratios, bottom=bottom, top=top, left=left, right=right)
    gs.update(wspace=wspace, hspace=0) # set the spacing between axes.
    for j in range(len(eps_lab)):
        ax0 = fig.add_subplot(gs[0, j])
        plot_data(ax0, eps_lab[j])
        ax1 = fig.add_subplot(gs[2, j])
        coefficient_box_plot(ax1, j, eps_lab, coef_values, mw, coefficient_list, ylim_boxplots)    
        if j!=0:
            ax1.set_yticklabels([])
        if j==0:
            ax1.set_ylabel("coefficient value")

        if show_epsilon:
            ax1.axhline(y=1-eps_lab[j], color='b', linestyle="dashed", label=r'$1-\epsilon$', zorder=2)
            if j==(len(eps_lab)-1):
                ax1.legend(loc=legend_loc)
    ax3 = fig.add_subplot(gs[4, 0:(len(eps_lab))])
    if noise_type == noise_generators.independent_noise:
        xlabel = r'$\epsilon$'
    else:
        xlabel = r'$\sigma$'
    plot_power(ax3, noise_lab, coefficient_list, beta, eps_lab, alpha, xlabel=xlabel)
    plt.savefig(plot_file_name)

plot(show_epsilon) 
