import unittest
import numpy as np
from scipy import stats 
from transport_dependency import tcor, mtcor, permutation_test # from here
from transport_dependency.min_cost_flow import ot_solver
from transport_dependency.extra_tools import estimate_power, other_coeffs, lower_bound

# Note, many of these tests rely on the numpy set seed functionality

class TestSum(unittest.TestCase):

    def test_tcor_basic(self):
        """Just some basic sanity test"""

        # maximal dependency
        np.random.seed(0)
        d = 1
        n = 3
        # x = np.random.multivariate_normal(np.zeros(d), np.eye(d), n)
        x = np.array([[1], [2], [3]])
        y = 2*x # + np.array([7, 2])
        res = tcor(x, y)
        np.testing.assert_almost_equal(res, 1, err_msg="should be 1", decimal=5)

        # just some already computed value
        x = [[1, 2, 3],
             [4, 5, 6],
             [7, 8., 9]]
        y = [1, 0, 0.3]
        res = tcor(x, y)
        np.testing.assert_almost_equal(res, 0.75, err_msg="tcor function is not working correctly", decimal=5)

    def test_tcor_arguments(self):
        np.random.seed(0)
        n = 10
        x = np.random.random(size=(n, 2))
        y = np.random.random(n)
        res = tcor(x, y, p=2, r=1.3, d_x="cityblock")
        np.testing.assert_almost_equal(res, 0.3951076246505389,
                err_msg="tcor function is not working correctly")

    def test_ot_solver(self):
        a = [3, 3, 3]
        b = [1, 1, 1, 1, 1, 1, 1, 1, 1]
        c = [[2.25 , 1.575 ,0. ,   3.375 ,2.7,  1.125, 4.5,   3.825, 2.25],
             [1.125, 1.8,   3.375 ,0.,     0.675, 2.25,  1.125, 1.8, 3.375],
             [2.925, 2.25,  3.825, 1.8,   1.125, 2.7,   0.675, 0.,    1.575]]
        transport_cost = ot_solver(a, b, c)
        np.testing.assert_almost_equal(transport_cost, 6.75, err_msg="ot solver not working", decimal=5)


    def test_mtcor(self):
        np.random.seed(0)
        d = 2
        n = 9
        x = np.random.randint(0, 4, size=n) # some categorical values for x
        y = np.random.multivariate_normal(np.zeros(d), np.eye(d), n)
        # a very hight alpha value approximates the marginal transport dependency well (in particular for categorical data)
        alpha = 100
        res_mtcor_approx = tcor(x, y, alpha=alpha)
        res_mtcor = mtcor(x, y)
        np.testing.assert_almost_equal(np.abs(res_mtcor - res_mtcor_approx), 0, err_msg="mtcor function is not working correctly", decimal=5)

    def test_permutation_test(self):
        np.random.seed(0)
        pearson = lambda x, y : np.abs(stats.pearsonr(x, y)[0]) 
        coef = pearson
        x = np.random.random(6)
        y = np.random.random(6)
        p_val = permutation_test(x, y, exact_level=True, r=3, use_multiprocessing=False)["p_val"]
        np.testing.assert_almost_equal(p_val, 0.31, err_msg="permutation test is not working correctly")

    def test_permutation_test_parallel(self):
        np.random.seed(0)
        pearson = lambda x, y : np.abs(stats.pearsonr(x, y)[0]) 
        coef = pearson
        x = np.random.random(6)
        y = np.random.random(6)
        p_val = permutation_test(x, y, exact_level=True, r=3, use_multiprocessing=True)["p_val"]
        np.testing.assert_almost_equal(p_val, 0.31, err_msg="permutation test is not working correctly")

    def test_permutation_test_power(self):
        np.random.seed(0)
        pearson = lambda x, y : np.abs(stats.pearsonr(x, y)[0]) 
        n = 6
        def data_gen_func():
            x = np.random.random(n)
            y = np.random.random(n) # for a dependent alternative you can use y = x + np.random.random(n)
            return x, y

        n_power = 100 # number of trials to estimate power of test
        coef = pearson
        level = 0.25
        m = 3
        power = estimate_power.estimate_power_of_permutation_test(coef, data_gen_func, level, m, n_power)
        np.testing.assert_almost_equal(power, 0.27, err_msg="permutation test is not working correctly")

    def test_pivot_revers(self):
        np.random.seed(1)
        n = 7
        x = np.array([0, 1, 2, 2, 0])*2
        y = np.array([0, 2, 2, 1, 1])*2
        res1 = tcor(x, y, solver="pivot")
        res2 = tcor(y, x)
        np.testing.assert_almost_equal(res1,  res2, err_msg="pivot solver is broken", decimal=5)

    def test_tcor_approximation(self):
        np.random.seed(1)
        n = 500
        x = np.random.random(size=n)
        y = np.random.random(size=n)
        res = tcor(x, y, estimation_method="permutation")
        np.testing.assert_almost_equal(res,  0.12203324230823102,
                err_msg="permutation based esimator not working", decimal=5)

    def test_pivot(self):
        np.random.seed(1)
        n = 3
        x = np.random.random(size=n)
        y = np.random.random(size=n)
        res1 = tcor(x, y, solver="pivot")
        res2 = tcor(x, y)
        np.testing.assert_almost_equal(res1,  res2, err_msg="pivot solver is broken", decimal=5)

    def test_dcor(self):
        np.random.seed(1)
        n = 500
        x = np.random.random(size=n)
        y = np.random.random(size=n)
        res = other_coeffs.dcor(x, y)
        np.testing.assert_almost_equal(res,  0.0450879, err_msg="dcor is broken", decimal=5)

    def test_lower_bound(self):
        np.random.seed(1)
        n = 50
        x = np.random.random(size=(n, 1))
        y = np.random.random(size=n)
        res = lower_bound.lb(x, y)
        np.testing.assert_almost_equal(res, 0.07531208, err_msg="lower bound is broken", decimal=5)

if __name__ == '__main__':
    unittest.main()
