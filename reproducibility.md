# Reproducibility of Figures and Simulations

This document provides instructions on how to reproduce some of the simulations and figures in [1]. As a first step it is necessary to install the python package ```transport_dependency``` as described in the ```README.md``` file. As some of the simulations are computationally quite expensive, it is recommended to use an high-performance server. For the reproduciblity, some addittional python packages are required. A list of the packages used can be found in ```simulations_for_paper/additional_requirements.txt```.

## Reproduce Table 1

Scripts to recreate Table 1 are located in the folder ```tree example```. To generate the values for Table 1, execute the script ```perform_analysis.py``` followed by ```data_for_table.py```.

This example uses the same datasets as in [2], that is, a gene correlation matrix, stored in ```data/correlation_cost_matrix_x.csv``` and the response variable values, stored in ```data/processed_data.csv```. We obtained such dataset by executing the R script ```get_data.R``` and the Python script ```process_data.py```. These scripts replicate the data-processing steps of [2] and thier code, available at https://github.com/merlebehr/treeSegresulting.

## Reproduce Figure 9
Run the Python scripts ```simulations_for_paper/fig9_lower.py``` and ```simulations_for_paper/fig9_upper.py``` to reproduce Figure 9. The figures will be generated in the directory ```simulation_for_paper/figures/```.

## Reproduce Figure 14
Execute the script ```simulations_for_paper/fig14.py``` to reproduce Figure 14. Due to computational intensity, it is recommended to run this simulation on a high-performance server. The resulting figures will be stored in ```simulation_for_paper/figures/```.

## Reproduce Figures 4, 10, 11, and all Figures in Appendix D
Use the script ```simulations_for_paper/runall.py``` to recreate Figures 4, 10, 11, and all figures in Appendix D.  The figures will be saved in the directory ```simulation_for_paper/figures/```. Individual simulations can be replicated using the script ```simulation_for_paper/command_plot.py```. It is recommended to run these simulations on a server.

[1] Nies, Thomas Giacomo, Thomas Staudt, and Axel Munk. "Transport dependency: Optimal transport based dependency measures." arXiv preprint arXiv:2105.02073 (2023).

[2] Behr M, Ansari MA, Munk A, Holmes C. Testing for dependence on tree structures. Proc Natl Acad Sci U S A. 2020 May 5;117(18):9787-9792. doi: 10.1073/pnas.1912957117. Epub 2020 Apr 22. PMID: 32321827; PMCID: PMC7211961.
