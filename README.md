# Transport Dependency

This package provides a few simple functions to compute the transport correlation and other optimal transport based dependency measures as defined here: [Transport dependency: Optimal transport based dependency measures](https://arxiv.org/abs/2105.02073).
Moreover, as specified in ```reproducibility.md```, it is possible to reproduce some of the numerical siumulations perfomred in the paper. 

#### Pip installation

To locally install the ```transport_dependency``` package it is sufficient to run

```console
git clone https://gitlab.gwdg.de/thomas.nies/transport-dependency.git
cd transport_dependency
pip install .
```

#### Short example

```python
# Compute transport correlation and test for independence
from transport_dependency import tcor, permutation_test
x = [[1, 2, 3],
     [4, 5, 6],
     [7, 8., 9]]
y = [1, 0, 0.3]
res = tcor(x, y)
test = permutation_test(x, y, coef=tcor)
p_val = test["p_val"]
```

#### References

[1] Nies, Thomas Giacomo, Thomas Staudt, and Axel Munk. "Transport dependency: Optimal transport based dependency measures." arXiv preprint arXiv:2105.02073 (2023).

